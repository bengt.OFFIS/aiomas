import os
import tempfile

import py.path
import pytest


@pytest.yield_fixture
def short_tmpdir():
    with tempfile.TemporaryDirectory() as tdir:
        yield py.path.local(tdir)


@pytest.fixture
def certs():
    certs = ['ca.pem', 'device.pem', 'device.key']
    cert_dir = os.path.join(os.path.dirname(__file__), 'certs')
    return [os.path.join(cert_dir, f) for f in certs]
