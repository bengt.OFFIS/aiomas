import io

import pytest

import aiomas.clocks
import aiomas.util


@pytest.mark.parametrize('path, expected', [
    ('io:StringIO', io.StringIO),
    ('io:StringIO.getvalue', io.StringIO.getvalue),
    ('aiomas.clocks:BaseClock', aiomas.clocks.BaseClock),
    ('aiomas.clocks:BaseClock.time', aiomas.clocks.BaseClock.time),
])
def test_obj_from_str(path, expected):
    obj = aiomas.util.obj_from_str(path)
    assert obj is expected


def test_obj_from_str_invalid_sep():
    excinfo = pytest.raises(ValueError, aiomas.util.obj_from_str, 'foo')
    assert str(excinfo.value) == ('Malformed object name "foo": Expected '
                                  '"module:object"')


def test_obj_from_str_importerror():
    pytest.raises(ImportError, aiomas.util.obj_from_str, 'spam:eggs')


def test_obj_from_str_objnotfound():
    pytest.raises(AttributeError, aiomas.util.obj_from_str, 'io:eggs')
